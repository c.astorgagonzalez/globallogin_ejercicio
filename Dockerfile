FROM adoptopenjdk:11-jre-hotspot

EXPOSE 8080

RUN mkdir /app

COPY /RestService-0.0.1-SNAPSHOT.jar  /app/RestService-0.0.1-SNAPSHOT.jar

ENTRYPOINT ["java","-jar","/app/RestService-0.0.1-SNAPSHOT.jar"]