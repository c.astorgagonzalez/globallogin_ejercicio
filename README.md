GlobalLogin_Ejercicio

# Diagramas realizados
1. https://gitlab.com/c.astorgagonzalez/globallogin_ejercicio/-/raw/master/src/main/resources/static/imagenes/Diagrama%20de%20Secuencia.jpg
2. https://gitlab.com/c.astorgagonzalez/globallogin_ejercicio/-/raw/master/src/main/resources/static/imagenes/Diagrama%20de%20componentes.jpg

# Configuracion de servicio.
1. Para iniciar el servicio se debe configurar el endpoint de la base de datos y definir que base de datos de destino utilizar dentro del archivo **application.properties**, este esta ubicado en src/resources/application.properties
2. Parametros importantes:
```
spring.datasource.url= jdbc:mysql://localhost:3306/schema?useSSL=false
spring.datasource.username= root
spring.datasource.password= root

spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5InnoDBDialect
spring.jpa.hibernate.ddl-auto = update -- Actualiza los campos, si no existe los crea.
#spring.jpa.hibernate.ddl-auto=create-drop -- Para eliminar y crear de nuevo la BD
#spring.jpa.hibernate.ddl-auto= none -- No genera ningun cambio en la BD
```

# Url del servicio
> localhost:8080/api/users/auth/register
```
Json Request
{
    "name": "nombre",
    "email": "correo@correo.cl",
    "password": "Password123",
    "phones": [
        {
            "number": 12345678,
            "cityCode": 2,
            "countryCode": 56
        }
    ]
}
```
#Response
>En caso de un registro correcto se recibira el siguiente Json Response
```
{
    "user": {
        "created": "2020-09-03T15:19:08.135250Z",
        "modified": "2020-09-03T15:19:08.135250Z",
        "id": 2,
        "name": "nombre",
        "email": "correo@correo.cl",
        "password": "$2a$10$NEirz4tlMoJCSG8f5/Y4v.OjAc4B8k2XaKmF/4kfYt51ufjBW3hsW",
        "active": true
    },
    "token": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIyIiwiaWF0IjoxNTk5MTQ2MzQ4LCJleHAiOjE1OTkxNDYzNzh9.joK2n2OeYJQi1IiMssurl9XwBhx4aEUkCTXznAOT6FG-nn3hhvIlDjJMhHWLNpXdrRjolQ83zGUrjbXhPr2xQA"
}
```
>Formatos de correo y contraseña
```
Correo
{
    "message": "Formato incorrecto de correo."
}

Contraseña
{
    "message": "Formato incorrecto, debe seguir la sieguiente regla: (Una Mayúscula, letras minúsculas, y dos números)"
}
```
>Correo ya utilizado
```
{
    "message": "Correo electronico ya registrado"
}
```
#URL para pruebas
>[174.138.108.5:8080/api/users/auth/register](174.138.108.5:8080/api/users/auth/register)
