package ejercicio.globallogic.RestService.models.audits;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
    value ={"created","modified"},
    allowGetters = true 
)
public abstract class DateAudit implements Serializable{
    
    /**
     *
     */
    private static final long serialVersionUID = 4812870529294092669L;

    @CreatedDate
    private Instant created;
    
    @LastModifiedDate
    private Instant modified;

    /**
     * @return Instant return the createdAt
     */
    public Instant getCreated() {
        return created;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Instant created) {
        this.created = created;
    }

    /**
     * @return Instant return the updateAt
     */
    public Instant getModified() {
        return modified;
    }

    /**
     * @param updateAt the updateAt to set
     */
    public void setModified(Instant modified) {
        this.modified = modified;
    }

}