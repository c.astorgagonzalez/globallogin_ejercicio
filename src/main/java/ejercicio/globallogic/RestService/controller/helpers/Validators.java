package ejercicio.globallogic.RestService.controller.helpers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validators {

    public static boolean validatePassword(String passwd) {
        String regex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{4,20}$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(passwd);
        return m.matches();
    }

    public static boolean isValidEmail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

}